//
// Header files
//


#ifndef NTUPLEMAKER_NTUPLEMAKE_H
#define NTUPLEMAKER_NTUPLEMAKE_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "AthenaBaseComps/AthAlgTool.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/ReadHandle.h"
#include "CaloEvent/CaloCellContainer.h"
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"
#include "xAODTrigL1Calo/TriggerTowerContainer.h"
#include "xAODTrigL1Calo/JGTowerContainer.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/IMatchingTool.h"
#include "AsgTools/AnaToolHandle.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODEventShape/EventShape.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "TTree.h"
//#include "CaloInterface/ICaloNoiseTool.h"
#include "CaloConditions/CaloNoise.h"
#include "StoreGate/ReadCondHandleKey.h"
#include "CaloGeoHelpers/CaloSampling.h"
using namespace std;

class NTupleMaker : public :: AthAnalysisAlgorithm { 

public: 

	NTupleMaker( const std::string& name, ISvcLocator* pSvcLocator );
	
	virtual ~NTupleMaker();
	
	virtual StatusCode initialize();
	virtual StatusCode execute();
	virtual StatusCode finalize();
	
public: 
            std::string m_CellContainerName;
            std::string m_SCellContainerName;
            
            bool       m_useSC;
            bool 	m_isMC;
            bool  	m_saveAllInfo;
            bool 	m_saveTowers;
            bool       m_saveimage;
            bool       m_saveCells;
            bool       m_applyNoiseCorrection;
            bool       m_doTwoGaussianNoise;
            float      m_absNoiseThreshold;
            float      m_negNoiseThreshold;
            
            std::vector<std::string> m_HLT_MET;
            std::vector<std::string> m_Triggers;
            std::vector<std::string> m_Truth_MET = {"NonInt", "Int", "IntMuons", "NonIntPlusIntMuons", "NonIntMinusIntMuons"};
            std::vector<int> m_CaloSampling;
private: 

	
	
	TTree* m_tree = 0;
	
	std::vector<double> cells_et;	
	std::vector<double> cells_eta;
	std::vector<double> cells_phi;
	std::vector<double> cells_t;
	std::vector<int>   cells_q;
	std::vector<int>   cells_smpl;
	std::vector<bool>  cells_pT;
	std::vector<bool>  cells_PF;
	
	std::map<TString, double> hlt_met_ex;
	std::map<TString, double> hlt_met_ey; 
	std::map<TString, double> hlt_met_et;  
	
	std::map<TString, double> truth_met_ex;
	std::map<TString, double> truth_met_ey; 
	std::map<TString, double> truth_met_et; 
	
	std::map<TString, TH2D> SamplingTH2D;
	std::map<TString, std::vector<double>> SamplingImage;
            
	double mu;
	int   evtNumber;
	double evtWeight;
	double evtLiveTime;
	double evtDensity;
	double evtDensitySigma;
	double evtArea;
	bool  isGoodLB = true;
	double distFrontBunchTrain; //

	double evtCellsEt;
	double evtCellsEx;
	double evtCellsEy;
    
        int* TrigDecisions;
        
        unsigned int m_NSamplings = CaloSampling::FCAL2+1;

private: 
	ToolHandle<Trig::TrigDecisionTool> m_TrigDecTool;        
	ToolHandle<Trig::IBunchCrossingTool> m_bcTool;   
	//ToolHandle<ICaloNoiseTool> m_noiseTool;
	
	SG::ReadCondHandleKey<CaloNoise> m_totalNoiseKey
    { this, "TotalNoiseKey", "totalNoise", "SG key for total noise" };

	
private: 
	
	inline void clear() {
	
		cells_et.clear();	
        	cells_eta.clear();
        	cells_phi.clear();
        	cells_t.clear();
        	cells_q.clear();
        	cells_smpl.clear();
        	cells_pT.clear();
        	cells_PF.clear();
		
	}
	
	StatusCode RetrieveHLTMET(TString name);
	void RetrieveTruthMET(const std::string &term, const xAOD::MissingET &met);  
};

#endif //> !NTUPLEMAKER_NTUPLEMAKER_H
