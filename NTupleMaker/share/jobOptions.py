
from PyUtils import AthFile

files=['/eos/user/m/mobelfki/AOD/ESD/mc21_13p6TeV/ESD.28877240._000107.pool.root.1']

jps.AthenaCommonFlags.FilesInput = files 

jps.AthenaCommonFlags.HistOutputs = ["OUTPUT:NTupleMaker_Zmumu_13p6TeV.root"]

#Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "POOLAccess" 

theApp.EvtMax=10


af = AthFile.fopen(svcMgr.EventSelector.InputCollections[0])
isMC = 'IS_SIMULATION' in af.fileinfos['evt_type']
run_number = af.run_number[0]

from AthenaCommon.AlgSequence import AlgSequence
topSequence=AlgSequence()

from AthenaCommon.GlobalFlags import globalflags


from RecExConfig.InputFilePeeker import inputFileSummary
globalflags.DataSource = 'data' if inputFileSummary['evt_type'][0] == "IS_DATA" else 'geant4'
globalflags.DetDescrVersion = inputFileSummary['geometry']


from RecExConfig import AutoConfiguration
AutoConfiguration.ConfigureSimulationOrRealData() #configures DataSource global flag
AutoConfiguration.ConfigureGeo()
AutoConfiguration.ConfigureConditionsTag() #sets globalflags.ConditionsTag
from AthenaCommon.DetFlags import DetFlags
DetFlags.all_setOff() #skip this line out to leave everything on. But this will take longer
DetFlags.detdescr.Calo_setOn() #e.g. if I am accessing CaloCellContainer, I need the calo detector description
include("RecExCond/AllDet_detDescr.py")

from METUtilities.METMakerConfig import getMETMakerAlg
metAlg = getMETMakerAlg('AntiKt4EMTopo',"Loose")
metAlg.METName = 'MET_Reco_AntiKt4EMTopo'

ToolSvc += CfgMgr.JetCalibrationTool("myJESTool")
ToolSvc.myJESTool.IsData=False
ToolSvc.myJESTool.ConfigFile="JES_MC16Recommendation_Aug2017.config"
ToolSvc.myJESTool.CalibSequence="JetArea_Residual_EtaJES_GSC"
ToolSvc.myJESTool.JetCollection="AntiKt4EMTopo" 
#ToolSvc += CfgMgr.Trig__MatchingTool("MyMatchingTool",OutputLevel=DEBUG)

ToolSvc += CfgMgr.Trig__MatchingTool("MyMatchingTool",OutputLevel=DEBUG)
ToolSvc += CfgMgr.CP__MuonSelectionTool("MediumMuonTool", MuQuality=1) # 1 corresponds to Medium
ToolSvc += CfgMgr.JetVertexTaggerTool('JVT')

ToolSvc += CfgMgr.Trig__TrigDecisionTool( "TrigDecisionTool" )


from CaloTools.CaloNoiseCondAlg import CaloNoiseCondAlg
theCaloNoiseTool=CaloNoiseCondAlg(noisetype="totalNoise")


#from TrigBunchCrossingTool.BunchCrossingTool import BunchCrossingTool
#if isMC: ToolSvc += CfgMgr.BunchCrossingTool( "MC" )
#else: ToolSvc += CfgMgr.BunchCrossingTool( "LHC" )


# Configure electron tool




####################################
##### Configuration
#####################################
L1_Triggers_list  = []
L1_Triggers_list  += ["L1_XE30", "L1_XE300", "L1_XE35", "L1_XE40", "L1_XE45", "L1_XE50", "L1_XE55", "L1_XE60"]
L1_Triggers_list += ["L1_J100", "L1_J15", "L1_J20", "L1_J40", "L1_J400", "L1_J50"]
#L1_Triggers_list += ["L1_J40_XE50", "L1_J40_XE60"]

HLT_Triggers_list  = []
HLT_Triggers_list += ["HLT_xe110_mht_L1XE50", "HLT_xe110_pfsum_L1XE50", "HLT_xe110_tc_em_L1XE50", "HLT_xe110_tcpufit_L1XE50"]
HLT_Triggers_list += ["HLT_xe30_cell_L1XE30", "HLT_xe30_mht_L1XE30", "HLT_xe30_tcpufit_L1XE30", "HLT_xe30_trkmht_L1XE30"]
HLT_Triggers_list += ["HLT_j100_pf_ftf_bdlr60_xe50_cell_xe85_tcpufit_L1XE55", "HLT_j55_0eta240_cell_L1J30_EMPTY"]
HLT_Triggers_list += ["HLT_j15", "HLT_j20", "HLT_j25", "HLT_j35", "HLT_j45", "HLT_j60", "HLT_j85", "HLT_j110", "HLT_j175", "HLT_j260", "HLT_j360", "HLT_j400", "HLT_j420", "HLT_j440",]

Triggers_list = L1_Triggers_list + HLT_Triggers_list

HLT_MET_list = ["HLT_MET_cell", "HLT_MET_pfopufit", "HLT_MET_tcpufit", "HLT_MET_mht", "HLT_MET_trkmht", "HLT_MET_tc", "HLT_MET_tc_em"]


Calo_Samplings = [0, 1, 2, 3, 4]

m_useSuperCells = False
m_isMC          = isMC
m_saveAllInfo   = False
m_SaveImage     = True
m_saveCells     = True
m_applyNoiseCorrection = False

topSequence += CfgMgr.NTupleMaker(CellContainerName = "AllCalo", SCellContainerName = "AllCalo", useSuperCells = m_useSuperCells, saveAllInfo = m_saveAllInfo,   HLT_MET = HLT_MET_list, Triggers = Triggers_list, isMC = m_isMC, SaveImage = m_SaveImage, SaveCells = m_saveCells, applyNoiseCorrection = m_applyNoiseCorrection, CaloSampling = Calo_Samplings)  


include("AthAnalysisBaseComps/SuppressLogging.py")
